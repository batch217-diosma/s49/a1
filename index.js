
// GET POST

fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPost(data))

// SHOW POST

const showPost = (post) => {
	let postEntries = ""

	post.forEach((post) => {
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost('${post.id}')">Edit</button>
				<button onclick = "deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries
}

// ADD POST

document.querySelector("#form-add-post").addEventListener("submit" , (e) => {

	e.preventDefault()

	fetch("https://jsonplaceholder.typicode.com/posts" , {
		method : "POST",
		body : JSON.stringify({
			title : document.querySelector("#txt-title").value,
			body : document.querySelector("#txt-body").value,
			userId : 1
		}),
		header : {"Content-type" : "application/json; charset = UTF-8"}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		alert("Successfully added.")

		document.querySelector("#txt-title").value = null
		document.querySelector("#txt-body").value = null
	})

})

// EDIT POST

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id
	document.querySelector("#txt-title-edit").value = title
	document.querySelector("#txt-body-edit").value = body
	document.querySelector("#btn-submit-update").removeAttribute('disabled' , true)

}

// UPDATE POST

document.querySelector("#form-edit-post").addEventListener('submit' , (e) => {
	e.preventDefault()

	fetch("https://jsonplaceholder.typicode.com/posts/1" , {
		method : "PUT",
		body : JSON.stringify({
			id : document.querySelector("#txt-edit-id").value,
			title : document.querySelector("#txt-title-edit").value,
			body : document.querySelector("#txt-body-edit").value,
			userId : 1
		}),
		header : {"Content-type" : "application/json; charset=UTF-8"}
	})
	.then((response) => response.json())
	.then((data) =>
	{
		console.log(data)
		alert("Successfully updated")

		document.querySelector("#txt-edit-id").value = null
		document.querySelector("#txt-title-edit").value = null
		document.querySelector("#txt-body-edit").value = null
		document.querySelector("#btn-submit-update").setAttribute('disabled' , true)
		
	})

})

// DELETE POST

const deletePost = (id) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}` , {method : "DELETE"
})
	document.querySelector(`#post-${id}`).remove()

}